var FPS = 60;
var canvas, ctx;
var ancho = 700;
var alto = 300;
var suelo = 200;
var trex = {y:suelo, vy: 0, vymax: 9, gravedad: 2, salto: 28, saltando: false};
var nivel = {velocidad: 9, puntuacion: 0, muerto: false};
var cactus = {x: ancho + 100, y: suelo-25};

document.addEventListener('keydown', function(evento) {
    if (evento.keyCode == 32) {
        if (nivel.muerto == false) {
            if (trex.saltando == false) {
                saltar();
            }
        } else {
            nivel.velocidad = 9;
            nivel.muerto = false;
            cactus.x = ancho + 100;
            nivel.puntuacion = 0
        }
    }
});

function inicializa() {
    canvas = document.getElementById('canvas');
    ctx = canvas.getContext('2d');
}

function borraCanvas() {
    canvas.width = ancho;
    canvas.height = alto;
}

function dibujaRex() {
    ctx.fillRect(100, trex.y, 50, 50);
}

function dibujaCactus() {
    ctx.fillRect(cactus.x, cactus.y, 38, 75);
}

function logicaCactus() {
    if (cactus.x < -100) {
        cactus.x = ancho + 100;
        nivel.puntuacion++;
    } else {
        cactus.x -= nivel.velocidad;
    }
}

function saltar() {
    trex.saltando = true;
    trex.vy = trex.salto;
}

function gravedad() {
    if (trex.saltando == true) {
        if (trex.y - trex.vy - trex.gravedad > suelo) {
            trex.saltando = false;
            trex.vy = 0;
            trex.y = suelo;
        } else {
            trex.vy -= trex.gravedad;
            trex.y -= trex.vy;
        }
    }
}

function colision() {
    if (cactus.x >= 100 && cactus.x <= 150) {
        if (trex.y > suelo-25) {
            nivel.muerto = true;
            nivel.velocidad = 0;
        }
    }
}

function puntuacion() {
    ctx.font = "30px impact";
    ctx.fillStyle = "#555555";
    ctx.fillText(`${nivel.puntuacion}`, 600, 50);

    if (nivel.muerto == true) {
        ctx.font = "60px impact";
        ctx.fillText('GAME OVER', 240, 150);
    }
}

setInterval(function() {
    principal();
}, 1000/FPS);

function principal() {
    borraCanvas();
    gravedad();
    colision();
    logicaCactus();
    dibujaRex();
    dibujaCactus();
    puntuacion();
}
